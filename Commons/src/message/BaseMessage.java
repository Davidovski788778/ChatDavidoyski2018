package message;

public class BaseMessage {

    public static final int MESSAGE_TYPE_PUBLIC = 1;
    public static final int MESSAGE_TYPE_SERVICE = 2;

    private int type;
    private long creationDate;
    private String messageBody;

    public BaseMessage(int type, long creationDate, String messageBody) {
        this.type = type;
        this.creationDate = creationDate;
        this.messageBody = messageBody;
    }


    public int getType() {
        return type;
    }
    public long getCreationDate() {
        return creationDate;
    }
    public String getMessageBody() {
        return messageBody;
    }


    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }
}

