package message;

import java.text.SimpleDateFormat;

public class PublicMessage extends BaseMessage{

    private String senderName;

    public String getSenderName() {
        return senderName;
    }
    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }


    public PublicMessage(String messageBody) {
        super(BaseMessage.MESSAGE_TYPE_PUBLIC,System.currentTimeMillis(),messageBody);
    }

    public PublicMessage(String messageBody, String senderName) {
        this(messageBody);
        this.senderName = senderName;
    }

    @Override
    public String toString(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MMM.yyyy ' at ' HH:mm:ss' :\n'");
        return senderName + " " + simpleDateFormat;
    }


}
