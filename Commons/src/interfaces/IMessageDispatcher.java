package interfaces;

import message.BaseMessage;

public interface IMessageDispatcher <T extends Thread, IClientsThread> {

    void registerClientsThread(T clients);
    void dispatchMessage(BaseMessage baseMessage);
    void messageDispatcherClose();
    void dispatchMessage(BaseMessage baseMessage, T clients);
}
